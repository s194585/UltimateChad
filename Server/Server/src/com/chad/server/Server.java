package com.chad.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.UUID;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

public class Server {
	
	public static String ip = "localhost";
	public static String port = "31420";
	
	public static final int latency = 1000;
	public static final int pingRate = 10000;
	
	public static final int maxClientCount = 4;
	
	public static String[][] playerInfoList= new String[maxClientCount][3];
	public static ArrayList<String> possibleColors = new ArrayList<String>(Arrays.asList("0","1","2","3","4","5","6"));
	public static String[] possibleWorlds = new String[] {"UltimateChadWorld1", "UltimateChadWorld2", "UltimateChadWorld3"};
	public static String currentWorld = "UltimateChadLobby";
	
	public static ArrayList<String> readyClients = new ArrayList<String>();
	
	public static ArrayList<String> connectedClientsIDs = new ArrayList<String>();
	
	public static void main(String[] args) {
		System.out.println("-|SERVER|-");
		
		// Create a repository for tuple spaces
		SpaceRepository serverRepository = new SpaceRepository();
		
		// Create a tuple for the lobby	
		SequentialSpace lobby = new SequentialSpace();
		
		// Add the lobby to the repository
		serverRepository.add("lobby", lobby);
		
		// Scanner for ip and port host
		Scanner serverScanner = new Scanner(System.in);
		
		
		System.out.println("Enter the hosting IP, \"local\" for localhost.");
		System.out.print("IP: ");
		ip = serverScanner.next();
		
		if(ip.equals("local")) {
			ip = "localhost";
		}
		
		System.out.println("Enter the hosting port, \"local\" for port \"31420\"");
		System.out.print("port: ");
		port = serverScanner.next();
		
		if(port.equals("local")) {
			port = "31420";
		}
		
		// Add a gate to the repository
		serverRepository.addGate("tcp://" + ip + ":" + port + "/?keep");
		
		// Print server online
		System.out.println("Server online, hosted on: " + ip + ":" + port);
		
		System.out.println("You can type \"exit\" to close the server anytime.");
		System.out.println();
		
		// Initialize butlerSpaceManager
		new Thread(new butlerSpaceManager(serverRepository, connectedClientsIDs, lobby, playerInfoList, possibleColors)).start();
		
		String input = serverScanner.next();
		if(input.equals("exit")) {
			System.out.println("Closing server.");
			System.exit(0);
		}
	}
	

	public static Object[] dataSwitch(Object[] clientOutput, String clientID) {
		String opcode = (String) clientOutput[0];
		
		switch(opcode) {
			case("clientReady"): 
				if(!readyClients.contains(clientID)) {
					readyClients.add(clientID);
				}
				if(readyClients.size() == connectedClientsIDs.size()) {
					readyClients.clear();
					currentWorld = chooseNextWorld();
					clientOutput[1] = currentWorld;

					return new Object[]{"distributeToAll", clientOutput};
				}else {
					return new Object[]{"notDistribute", clientOutput};
				}
				
			case("clientWon"):
				currentWorld = "UltimateChadLobby";
				clientOutput[1] = currentWorld;
				readyClients.clear();
				// Find the name of the player who won
				for(int i = 0; i < playerInfoList[0].length; i++) {
					if(clientID.equals(playerInfoList[i][0])) {
						clientOutput[2] = playerInfoList[i][1];
						break;
					}
				}
				System.out.println("*Winner event*");
				System.out.println("\t" + clientOutput[2].toString() + " won.");
				return new Object[]{"distributeToAll", clientOutput};
				
			default:
				return new Object[]{"distributeToAllOther", clientOutput};
		}
	
		
}


	private static String chooseNextWorld() {
		int i = (int) (Math.random()*possibleWorlds.length);
		return possibleWorlds[i];
	}
}


class butlerSpaceManager implements Runnable {
	private SpaceRepository serverRepository;
	private ArrayList<String> connectedClientsIDs;
	private Space lobby;
	private String[][] playerInfoList;
	public static ArrayList<String> possibleColors;

	
	public butlerSpaceManager (SpaceRepository serverRepository, ArrayList<String> connectedClientsIDs, Space lobby, String[][] playerInfoList, ArrayList<String> possibleColors) {
		this.serverRepository = serverRepository;
		this.connectedClientsIDs = connectedClientsIDs;
		this.lobby = lobby;
		this.playerInfoList = playerInfoList;
		this.possibleColors = possibleColors;

	}
	
	@Override
	public void run() {
		// Create the butlerConnectionManager
		new Thread(new butlerConnectionManager(serverRepository, connectedClientsIDs, playerInfoList, possibleColors)).start();
		
		while(true) {
			
			try {				
				// Listen for a new client connection
				Object[] name = lobby.get(new ActualField("clientConnect"), new FormalField(String.class));
				
				System.out.println("*New client connection event*");
				
				// Check if max client count is reached
				System.out.println("Connected clients: " + (connectedClientsIDs.size() + 1) + "/" + Server.maxClientCount);
				if(connectedClientsIDs.size() < Server.maxClientCount) {
					
					// Generate a unique id
					String uniqueID = UUID.randomUUID().toString();
					
					System.out.println("\tGenerated id " + uniqueID + " for client.");
					
					// Generate color for the new player
					String color = generateColor(possibleColors);
					
					// Store player info
					for(int i = 0; i < Server.maxClientCount;i++) {
						if(playerInfoList[i][0] == null) {
							playerInfoList[i][0] = uniqueID;
							playerInfoList[i][1] = (String) name[1];
							playerInfoList[i][2] = color;
							break;
						}
					}
					
					// Create in/out channels
					serverRepository.add(uniqueID + "In", new SequentialSpace());
					serverRepository.add(uniqueID + "Out", new SequentialSpace());
					
					// Send the id to the client
					lobby.put("clientID", uniqueID, color);
					
					// Notify all connected clients that a new client has connected
					for(int i = 0; i < connectedClientsIDs.size(); i++) {
						serverRepository.get(connectedClientsIDs.get(i) + "In").put("newClientConnection", uniqueID, name[1], color);
					}
									
					// Send all currently connected clients
					serverRepository.get(uniqueID + "In").put("connectedClients", Server.currentWorld, connectedClientsIDs, playerInfoList);
					
					// Add the new client ID to ID list
					connectedClientsIDs.add(uniqueID);
					System.out.println("\tConnected client list: " + connectedClientsIDs.toString());
					
					// Print player info list
					System.out.println("\tPlayer information list: " + Arrays.deepToString(playerInfoList));
					
					
					// Create a private butlerDistributer
					new Thread(new butlerDistributor(serverRepository, connectedClientsIDs, uniqueID)).start();
					
				} else {
					System.out.println("Too many clients, cannot connected more!");
					lobby.put("clientID", "errorTooManyClients", "noColor");
				}
				
			} catch (InterruptedException e1) {}
		}
	}
	
	public String generateColor(ArrayList<String> possibleColors) {
		String color = "";
		int rand = (int) (Math.random()*possibleColors.size());
		color = possibleColors.get(rand);
		possibleColors.remove(rand);
		return color;
	}
}


// Checks whether clients lost connection or disconnected
class butlerConnectionManager implements Runnable {
	private SpaceRepository serverRepository;
	private ArrayList<String> connectedClientsIDs;
	private ArrayList<String> clientsToBeDisconnected = new ArrayList<String>();
	private String[][] playerInfoList;
	private ArrayList<String> possibleColors;
	
	public butlerConnectionManager(SpaceRepository serverRepository, ArrayList<String> connectedClientsIDs, String[][] playerInfoList, ArrayList<String> possibleColors) {
		this.serverRepository = serverRepository;
		this.connectedClientsIDs = connectedClientsIDs;
		this.playerInfoList = playerInfoList;
		this.possibleColors = possibleColors;
	}
	
	@Override
	public void run() {
		while(true) {
			// Wait for one second
			try { 
				Thread.sleep(Server.pingRate); 
			} catch (InterruptedException e1) {}
			// Ping all connected clients
			for(int i = 0; i < connectedClientsIDs.size(); i++) {
				try {
					// Ping the client
					serverRepository.get(connectedClientsIDs.get(i) + "In").put(100, -999, -999, "connectionPing");
					
					Thread.sleep(Server.latency);
					
					// Check for response from clients
					Object pingBack = serverRepository.get(connectedClientsIDs.get(i) + "Out").queryp(new ActualField("pingToServer"), new FormalField(Object.class), new FormalField(Object.class), new ActualField("pingOK"));
					if(pingBack != null) {
						serverRepository.get(connectedClientsIDs.get(i) + "Out").get(new ActualField("pingToServer"), new FormalField(Object.class), new FormalField(Object.class), new ActualField("pingOK"));
					} else {
						System.out.println("Connection with " + connectedClientsIDs.get(i) + " was lost, triggering remove event..");
						// Store the disconnected clients
						clientsToBeDisconnected.add(connectedClientsIDs.get(i));
						
						// Remove stored info
						if(playerInfoList[i][0] != null && playerInfoList[i][0].equals(connectedClientsIDs.get(i))) {
							possibleColors.add(playerInfoList[i][2]);
							playerInfoList[i][0] = playerInfoList[i][1] = playerInfoList[i][2] = null;
						}
					}
					
				} catch (InterruptedException e) {}
			}
			
			// Remove the disconnected clients
			for(String ID : clientsToBeDisconnected) {
				// Send the client to disconnect to all other clients
				for(int i = 0; i < connectedClientsIDs.size(); i++) {
					if(!connectedClientsIDs.get(i).equals(ID)) {
						try {
							serverRepository.get(connectedClientsIDs.get(i) + "In").put("clientDisconnect", ID, -999, -999);
						} catch (InterruptedException e) {}
					}

				}
				connectedClientsIDs.remove(ID);
				Server.readyClients.remove(ID);
				
			}
			clientsToBeDisconnected.clear();
		}
	}
}





// Distributes incoming data to all clients currently connected to the server
class butlerDistributor implements Runnable {
	
	private SpaceRepository serverRepository;
	private ArrayList<String> connectedClientsIDs;
	private String clientID;
	
	public butlerDistributor(SpaceRepository serverRepository, ArrayList<String> connectedClientsIDs, String clientID) {
		this.serverRepository = serverRepository;
		this.connectedClientsIDs = connectedClientsIDs;
		this.clientID = clientID;
	}
	
	@Override
	public void run() {
		System.out.println("\tbutlerDistributer for " + clientID + " running.");
		
		Space clientOut = serverRepository.get(clientID + "Out");
		
		while(true) {
			Object[] clientOutput = null;
			String opcode = null;
			Object[] toDistribute = null;
			try {
				
				// Listen for the client output
				clientOutput = clientOut.get(new FormalField(String.class), new FormalField(Object.class), new FormalField(Object.class));
				opcode = (String) clientOutput[0];
				toDistribute = Server.dataSwitch(clientOutput, clientID);
			} catch (InterruptedException e) {}
			
			// Send the client output to every other connected client
			if(toDistribute[0].equals("distributeToAllOther")) {
				for(int i = 0; i < connectedClientsIDs.size(); i++) {
					if(!connectedClientsIDs.get(i).equals(clientID)) {
						try {
							serverRepository.get(connectedClientsIDs.get(i) + "In").put(opcode, clientID, ((Object[]) toDistribute[1])[1], ((Object[]) toDistribute[1])[2]);
						} catch (InterruptedException e) {}
					}
				}
			}else if(toDistribute[0].equals("distributeToAll")) {
				for(int i = 0; i < connectedClientsIDs.size(); i++) {
						try {
							serverRepository.get(connectedClientsIDs.get(i) + "In").put(opcode, clientID, ((Object[]) toDistribute[1])[1], ((Object[]) toDistribute[1])[2]);
						} catch (InterruptedException e) {}
				}
			}
		}
	}
}









