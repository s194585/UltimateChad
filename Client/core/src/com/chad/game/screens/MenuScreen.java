package com.chad.game.screens;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.chad.game.UltimateChad;

public class MenuScreen extends ApplicationAdapter implements Screen  {
	
	private Viewport viewPort;
    private Stage stage;
    private UltimateChad game;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Table table;
    private int menuHeight = 512;
    private int menuWidth = 670;
    private Skin skin;
    
    private String localIP = "localhost";
    private String localPort = "31420";


    public MenuScreen(UltimateChad game) {
    	this.game = game;
 
    	viewPort = new ExtendViewport(menuWidth, menuHeight, new OrthographicCamera());
    	batch = new SpriteBatch();
    	skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
    	
    	this.create();
    	
    	
    	
		
    }
    
    @Override
    public void create() {
    	stage = new Stage(viewPort, batch);
    	
    	Gdx.input.setInputProcessor(stage);
		
		
    	//Create table to hold the labels and textfields
    	table = new Table();
    	table.center();
    	table.setFillParent(true);
    	
    	
    	TextField nameField = addTextField("Name, e.g. \"Bob\"");
    	table.row();
        TextField serverField = addTextField("IP, e.g. \"192.168.0.1\"");
        table.row();
        TextField portField = addTextField("Port, e.g. \"31420\"");
        table.row();
        Button button = addButton("Play");
        
        button.addListener(new InputListener() {
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		// If button is pressed, run the serverlogic along with the playscreen
        		String playerName = "Player";
        		String IPv4 = localIP;
        		String port = localPort;
        		if(!nameField.getText().equals("Name, e.g. \"Bob\"")) {
        			playerName = nameField.getText();
        		}else {
        			playerName = "Player";
        		}
				if(!serverField.getText().equals("IP, e.g. \"192.168.0.1\"")) {
					IPv4 = serverField.getText();
				}
				if(!portField.getText().equals("Port, e.g. \"31420\"")) {
					port = portField.getText();
				}
				System.out.println(IPv4 + ", " + port);
				game.serverLogic(IPv4, port, playerName);
	            dispose();
				return true;
			}
        });
        
        // Add to stage
        stage.addActor(table);
        stage.draw();
    }
	
	
	@Override
	public void show() {
		
	}

	private TextButton addButton(String name) {
		TextButton button = new TextButton(name, skin);
		table.add(button).width(400).height(80).padTop(75);
		return button;
	}
	
	private TextField addTextField(String name) {
		TextField textField = new TextField(name, skin);
		textField.addListener(new ClickListener() {
			public void clicked (InputEvent e, float x, float y) {
				if(textField.getText().equals(name)) {
					textField.setText("");
				}
				
			}
		});
		
		
		table.add(textField).width(400).height(40).pad(20);
		return textField;
		
		
	}


	@Override
	public void render(float delta) {
		
		// Render the background black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
		
		
	}

	@Override
	public void resize(int width, int height) {
		width = menuWidth;
		height = menuHeight;
		viewPort.update(width, height);
		
	}

	@Override
	public void pause() {

		
	}

	@Override
	public void resume() {

		
	}

	@Override
	public void hide() {

		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	


}
