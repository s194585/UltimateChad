package com.chad.game.screens;

import java.util.ArrayList;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;
import org.jspace.SequentialSpace;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.chad.game.UltimateChad;
import com.chad.game.sprites.Player;
import com.chad.game.sprites.RemotePlayer;
import com.chad.scenes.Hud;
import com.chad.tools.ChadContactListener;

public class PlayScreen implements Screen {
	
	// Networking variables
	private SequentialSpace remoteConnectSpace;
	private RemoteSpace userOut;
	private RemoteSpace userIn;
	public ArrayList<RemotePlayer> connectedClients;
	
	// Game world variables
	private UltimateChad game;
	private World world;
	public static String currentWorld;
	public boolean lookForNewWorld = false;
	private Hud hud;
	public static String winnerName;
	public boolean speedRunEnabled = false;
	
	
	// Camera variables
	private OrthographicCamera gameCam;
	private Viewport gamePort;
	private TmxMapLoader mapLoader;
	private TiledMap tiledMap;
	private OrthogonalTiledMapRenderer mapRenderer;
	
	// Player variables
	private Box2DDebugRenderer b2dDebugRenderer;
	private Player player;
	
	// Remote player variables
	static Texture playerTextureRed;
	static Texture playerTextureBlue;
	static Texture playerTextureGreen;
	static Texture playerTextureYellow;
	static Texture playerTexturePink;
	static Texture playerTexturePurple;
	static Texture playerTextureGrey;
	
	//Player label elements
	private Label playerLabel;
	private Group groupPlayerLabels;
	private String playerName;
	
	//Declare bodies
	private BodyDef bdef;
	private PolygonShape shape;
	private FixtureDef fdef; 
	private Body body;
	private ArrayList<Body> bodyArray;
	
	
	//Declare music
	private Music speedRunMusic;
	private Music themeMusic;
	
	public PlayScreen (UltimateChad game, SequentialSpace remoteConnectSpace, RemoteSpace userIn, RemoteSpace userOut, String playerName, String newWorld) {
		
		//--- Start initializations
		
		// Game world
		currentWorld = newWorld;
		this.game = game;
		mapLoader = new TmxMapLoader();
		tiledMap = mapLoader.load("levels/" + currentWorld + ".tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(tiledMap, 1/ UltimateChad.PPM);
		hud = new Hud(game.batch);
		
		// Camera
		gameCam = new OrthographicCamera();
		gamePort = new FitViewport(game.viewWidth / UltimateChad.PPM, game.viewHeight / UltimateChad.PPM, gameCam);
		gameCam.position.set(game.viewWidth/2/UltimateChad.PPM, game.viewHeight/2/UltimateChad.PPM,0);
		
		// Networking
		connectedClients = new ArrayList<RemotePlayer>();
		this.userOut = userOut;
		this.userIn = userIn;
		
		
		// Create game world
		Vector2 gravity = new Vector2(0,-10);
		world = new World(gravity, true);
		
		
		// Remote player
		playerTextureRed = new Texture("playerTextures/playerRed.png");
		playerTextureBlue = new Texture("playerTextures/playerBlue.png");
		playerTextureGreen = new Texture("playerTextures/playerGreen.png");
		playerTextureYellow = new Texture("playerTextures/playerYellow.png");
		playerTexturePink = new Texture("playerTextures/playerPink.png");
		playerTexturePurple = new Texture("playerTextures/playerPurple.png");
		playerTextureGrey = new Texture("playerTextures/playerGrey.png");
		
		Texture[] textureList = {playerTextureRed, playerTextureBlue, playerTextureGreen, playerTextureYellow, playerTexturePink, playerTexturePurple, playerTextureGrey};
		
		this.remoteConnectSpace = remoteConnectSpace;
		new Thread(new butlerRemoteCreater(remoteConnectSpace, userIn, world, connectedClients, textureList)).start();
		
		
		
		// Player
		this.playerName = playerName;
		try {
			Object[] color = remoteConnectSpace.get(new ActualField("playerColor"), new FormalField(String.class));
			player = new Player(world, textureList[Integer.parseInt(color[1].toString())], playerName, this);
		} catch (InterruptedException e) {}
		 
		
		world.setContactListener(new ChadContactListener());
		
		
		//Player label
		LabelStyle labelStyle = new LabelStyle(new BitmapFont(), Color.BLACK);
		playerLabel = new Label("player", labelStyle);
		playerLabel.setFillParent(true);
	    groupPlayerLabels = new Group();
	    groupPlayerLabels.addActor(playerLabel);
	    groupPlayerLabels.setScale(1f/UltimateChad.PPM, 1f/UltimateChad.PPM);
				
		// Start sender butler
		new Thread(new butlerSender(player, userOut)).start();
		
		//--- End initializations
		
		// *Show the collision outlines for testing*
//		b2dDebugRenderer = new Box2DDebugRenderer();
		
		
		// Create music
		themeMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/thememusic.mp3"));
		speedRunMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/speedrunmusic.mp3"));
		
		themeMusic.setLooping(true);
		speedRunMusic.setLooping(true);
		
		speedRunMusic.setVolume(0.05f);
		themeMusic.setVolume(0.05f);
		
		
		themeMusic.play();
		
		declareHitbox();
		
	
	}
	
	// Notifies the server that the player is ready
	public void notifyServer(String opcode) {
		try {
			userOut.put(opcode, -999, -999);
			//lookForNewWorld = true;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	// Will get a world update from the server
	public void getNewWorld() {
		player.setWorld(currentWorld);
		
		if(PlayScreen.winnerName != null) {
			hud.setWinningClient("WINNER IS " + PlayScreen.winnerName);
		}
		
		lookForNewWorld = false;
	}
	
	public void setWorld(String nextWorld) {
		for(int i = 0; i < bodyArray.size(); i++) {
			world.destroyBody(bodyArray.get(i));
		}
		
		if(!nextWorld.equals("UltimateChadLobby")) {
			hud.setWinningClient("");
			hud.resetSpeedRunTime();
		}
		
		tiledMap = mapLoader.load("levels/" + nextWorld + ".tmx");
		mapRenderer.setMap(tiledMap);
		
		declareHitbox();
		
	}
	
	public void declareHitbox() {
			bodyArray = new ArrayList<Body>();
			bdef = new BodyDef();
			shape = new PolygonShape();
			fdef = new FixtureDef(); 
			
	
			// Add collisions to every object
			// GROUND LAYER , getLayers().get(2) means we get everything from the layer at index 2 in "Tiled"
			for(MapObject object : tiledMap.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)) {
				Rectangle rect = ((RectangleMapObject) object).getRectangle(); 
				
				bdef.type = BodyDef.BodyType.StaticBody;
				bdef.position.set((rect.getX() + rect.getWidth()/2)/UltimateChad.PPM, (rect.getY() + rect.getHeight()/2)/UltimateChad.PPM);
				
				body = world.createBody(bdef);
				bodyArray.add(body);
				
				shape.setAsBox(rect.getWidth()/2/game.PPM, rect.getHeight()/2/game.PPM);
				fdef.shape = shape;
				fdef.friction = 0.5f;
				body.createFixture(fdef);
				
			}
			
			// DEATH LAYER
			for(MapObject object : tiledMap.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {
				Rectangle rect = ((RectangleMapObject) object).getRectangle(); 
				
				bdef.type = BodyDef.BodyType.StaticBody;
				bdef.position.set((rect.getX() + rect.getWidth()/2)/UltimateChad.PPM, (rect.getY() + rect.getHeight()/2)/UltimateChad.PPM);
				
				body = world.createBody(bdef);
				bodyArray.add(body);
				
				shape.setAsBox(rect.getWidth()/2/game.PPM, rect.getHeight()/2/game.PPM);
				fdef.shape = shape;
				fdef.friction = 0.5f;
				
				body.createFixture(fdef).setUserData("death");
				
			}
			
			// WIN LAYER 
			for(MapObject object : tiledMap.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
				Rectangle rect = ((RectangleMapObject) object).getRectangle(); 
				
				bdef.type = BodyDef.BodyType.StaticBody;
				bdef.position.set((rect.getX() + rect.getWidth()/2)/UltimateChad.PPM, (rect.getY() + rect.getHeight()/2)/UltimateChad.PPM);
				
				body = world.createBody(bdef);
				bodyArray.add(body);
				
				shape.setAsBox(rect.getWidth()/2/game.PPM, rect.getHeight()/2/game.PPM);
				fdef.shape = shape;
				fdef.friction = 0.5f;
				
				body.createFixture(fdef).setUserData("win");
				
			}
			
			// CHOICE LAYER
			for(MapObject object : tiledMap.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
				Rectangle rect = ((RectangleMapObject) object).getRectangle(); 
				
				bdef.type = BodyDef.BodyType.StaticBody;
				bdef.position.set((rect.getX() + rect.getWidth()/2)/UltimateChad.PPM, (rect.getY() + rect.getHeight()/2)/UltimateChad.PPM);
				
				body = world.createBody(bdef);
				bodyArray.add(body);
				
				shape.setAsBox(rect.getWidth()/2/game.PPM, rect.getHeight()/2/game.PPM);
				fdef.shape = shape;
				fdef.friction = 0.5f;
				
				body.createFixture(fdef).setUserData("play");
				
			}
	}
	
	@Override
	public void show() {
		
	}
	
	public void update(float deltaTime) {
		// Update the keystrokes
		handleInput(deltaTime);
		
		// Create world physics
		world.step(1/60f, 6, 2);

		// Update player state
		player.update();
		
		// Get the new world if we are looking for one
		if(lookForNewWorld) {
			getNewWorld();
		}
		
		if(!currentWorld.equals("UltimateChadLobby") && speedRunEnabled) {
			hud.update(deltaTime);
		}
		
		
		// Update the camera
		gameCam.position.x = player.playerBody.getPosition().x;
		gameCam.position.y = player.playerBody.getPosition().y + 150/game.PPM;
		gameCam.update();
		mapRenderer.setView(gameCam);
		
		
		
	}

	private void handleInput(float deltaTime) {
        // Get the player key strokes, and move the player accordingly
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP) || Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            player.jump();
        }
        if((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) && player.playerBody.getLinearVelocity().x <= 4) {
        	player.walkRight();        }
        if((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A))  && player.playerBody.getLinearVelocity().x >= -4) {
            player.walkLeft();
        }
        
        if(this.currentWorld.equals("UltimateChadLobby") && Gdx.input.isKeyJustPressed(Input.Keys.F)) {
        	speedRunEnabled = !speedRunEnabled;
        	if(speedRunEnabled) {
        		themeMusic.stop();
            	speedRunMusic.play();
        	}else {
        		themeMusic.play();
            	speedRunMusic.stop();
        	}
        }
        
    }

	@Override
	public void render(float delta) {
		
		// Run update
		update(delta);
		
		// Render the background black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				
		// Render the map
		mapRenderer.render();
		
		// **Render collision outlines**
		//b2dDebugRenderer.render(world,gameCam.combined);
		
		
		hud.speedRunEnabled(speedRunEnabled);
		
		hud.stage.draw();
		
		
		// Initialize the sprite batch
		game.batch.setProjectionMatrix(gameCam.combined);
		
		// Draw to screen
		game.batch.begin();
		
		// Render client player
		player.render(game.batch);
		
		// Render the remote players
		for(RemotePlayer RP : connectedClients) {
			RP.draw(game.batch);
			playerLabel.setText(RP.getName());
			groupPlayerLabels.setPosition(RP.getX(), (float) (RP.getY() + RP.getHeight()*1.5));
			groupPlayerLabels.draw(game.batch, 1);
		}
		game.batch.end();

		
	}

	@Override
	public void resize(int width, int height) {
		gamePort.update(width,height);
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// Cleanup
		playerTextureRed.dispose();
		playerTextureBlue.dispose();
		playerTextureGreen.dispose();
		playerTextureYellow.dispose();
		themeMusic.dispose();
		speedRunMusic.dispose();
		player.dispose();
	}
	
	public void setRemotePlayerPosition(String ID, float posX, float posY) {
		for(int i = 0; i < connectedClients.size(); i++) {
			if(connectedClients.get(i) != null && connectedClients.get(i).getID().equals(ID)) {
				connectedClients.get(i).reDefineRemotePlayer(posX, posY);
			}
		}
	}
}

// Creates remote players, and assigns a butler
class butlerRemoteCreater implements Runnable{
	private SequentialSpace remoteConnectSpace;
	private RemoteSpace userIn;
	private World world;
	private ArrayList<RemotePlayer> remotePlayerList;
	private Texture[] textureList;
	
	public butlerRemoteCreater(SequentialSpace remoteConnectSpace, RemoteSpace userIn, World world, ArrayList<RemotePlayer> remotePlayerList, Texture[] textureList) {
		this.remoteConnectSpace = remoteConnectSpace;
		this.userIn = userIn;
		this.world = world;
		this.remotePlayerList = remotePlayerList;
		this.textureList = textureList;
	}
	
	@Override
	public void run() {
		
		while(true) {
			try {
				// Listen for a new client connection
				if(remoteConnectSpace.queryp(new ActualField("clientConnected"), new FormalField(String.class), new FormalField(String.class), new FormalField(String.class)) != null) {
					Object[] remoteInput = remoteConnectSpace.get(new ActualField("clientConnected"), new FormalField(String.class), new FormalField(String.class), new FormalField(String.class));
										
					// Create the remote player
					RemotePlayer remotePlayer = new RemotePlayer(world, (String) remoteInput[1], remoteInput[2].toString(), textureList[Integer.parseInt(remoteInput[3].toString())]);
					
					// Store the remote player
					remotePlayerList.add(remotePlayer);
				}
				
				// Listen for disconnects
				if(remoteConnectSpace.queryp(new ActualField("clientDisconnect"), new FormalField(String.class)) != null) {
					Object[] remoteInput = remoteConnectSpace.get(new ActualField("clientDisconnect"), new FormalField(String.class));
					
					// Remove remote player
					ArrayList<RemotePlayer> remotePlayersToBeRemoved = new ArrayList<RemotePlayer>();
					for(RemotePlayer RP : remotePlayerList) {
						if(RP.getID().equals(remoteInput[1])) {
							remotePlayersToBeRemoved.add(RP);
						}
					}
					for(RemotePlayer RP : remotePlayersToBeRemoved) {
						remotePlayerList.remove(RP);
					}
					remotePlayersToBeRemoved.clear();
				}
				
			} catch (InterruptedException e) {}
		}
	}
}


// Handles sending the players data to the server
class butlerSender implements Runnable {
	private Player player;
	private RemoteSpace userOut;
	
	public butlerSender(Player player, RemoteSpace userOut) {
		this.player = player;
		this.userOut = userOut;
	}
	
	@Override
	public void run() {
		int sendMore = 2;
		while(true) {
			// Send player position
			try {
				Thread.sleep(17);
				if(player.getState() != Player.State.IDLE) {
					Vector2 pos = player.playerBody.getPosition();
					userOut.put("positionUpdate", pos.x, pos.y);
					sendMore = 2;
				} else if(sendMore > 0 ) {
					Vector2 pos = player.playerBody.getPosition();
					userOut.put("positionUpdate", pos.x, pos.y);
					sendMore--;
				}
			} catch (InterruptedException e) {}
		}
	}
}