package com.chad.game.sprites;

//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.chad.game.UltimateChad;
import com.chad.game.screens.PlayScreen;

public class Player extends Sprite {
	 public World world;
	 public Body playerBody;
	   public enum State { IDLE, WALKINGRIGHT, WALKINGLEFT, JUMPING, FALLING};
	   public State currentState;
	   public State previousState;
	   public String Color;
	   private Sprite playerSprite;
	   public boolean hasDied;
	   private String playerName;
	   private PlayScreen playScreen;
	   public boolean setWorld;
	   private String updateWorld;
//	   private Sound jumpSound;
//	   private Sound deathSound;
	   
	 public Player(World world, Texture playerTexture, String playerName, PlayScreen playScreen) {
		 this.world = world;
		 this.playScreen = playScreen;
		 this.playerName = playerName;
		 hasDied = false;
		 //set starting states
	     currentState = State.IDLE;
	     //this.setScale(playerTexture.getWidth()/100f, (float)playerTexture.getHeight()/100f);
	     
	     playerSprite = new Sprite(playerTexture);
	     playerSprite.setSize(playerTexture.getWidth()/100f, (float)playerTexture.getHeight()/100f);
	     
	     this.set(playerSprite);
	     this.setTexture(playerTexture);
	     
	     // Create sounds
//	     jumpSound = Gdx.audio.newSound(Gdx.files.internal("sound/jumpsound.wav"));
//	     deathSound = Gdx.audio.newSound(Gdx.files.internal("sound/deathSound.wav"));
	     
		 definePlayer();
	 }

	public void definePlayer() {
		// Create the player body
		BodyDef bdef = new BodyDef();
		
		// Set the starting position
		bdef.position.set(96/UltimateChad.PPM, 96/UltimateChad.PPM);
		
		// Give the body a type
		bdef.type = BodyDef.BodyType.DynamicBody;
		
		// Create the body in the game world
		playerBody = world.createBody(bdef);
		
		// Give the body a fixture (collision box)
		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(15/UltimateChad.PPM);
		fdef.shape = shape;
		//fdef.isSensor = true;
		playerBody.createFixture(fdef).setUserData(this);
		
	}
	
	public void jump(){
		if ( currentState != State.JUMPING ) {
			playerBody.applyLinearImpulse(new Vector2(0, 6f), playerBody.getWorldCenter(), true);
//			jumpSound.play(0.1f);
            currentState = State.JUMPING;
        }
	}
	
	public void walkRight() {
		playerBody.applyLinearImpulse(new Vector2(0.2f, 0), playerBody.getLinearVelocity(), true);
		currentState = State.WALKINGRIGHT;
	}
	
	public void walkLeft() {
		playerBody.applyLinearImpulse(new Vector2(-0.2f, 0), playerBody.getLinearVelocity(), true);
		currentState = State.WALKINGLEFT;
	}
	
	public State getState(){
        if(playerBody.getLinearVelocity().y != 0) {
            return State.JUMPING;
        } else if(playerBody.getLinearVelocity().x > 0) {
            return State.WALKINGRIGHT;
        } else if(playerBody.getLinearVelocity().x < 0) {
            return State.WALKINGLEFT;
        } else {
            return State.IDLE;
        }
    }
	
	public void update() {
		currentState = getState();
		if(hasDied == true) {
			world.destroyBody(playerBody);
			hasDied = false;
			definePlayer();
		}
		if(setWorld == true) {
			playScreen.setWorld(updateWorld);
			setWorld = false;
			
		}
	}
	
	public void render(SpriteBatch batch) {
		// Get player physics position
		Vector2 playerPos = playerBody.getPosition();
		playerSprite.setPosition((float)(playerPos.x - playerSprite.getWidth()/2), (float)(playerPos.y - playerSprite.getHeight()/2));

	    // Draw the sprite
	    playerSprite.draw(batch);
	}
	
	public void killPlayer() {
		hasDied = true;
//		deathSound.play(0.5f);
	}
	
	
	public void setWorld(String nextWorld) {
		updateWorld = nextWorld;
		setWorld = true;
		hasDied = true;
	}
	
	public void notifyServer(String opcode) {
		playScreen.notifyServer(opcode);
	}
	
	public void dispose() {
//		deathSound.dispose();
//		jumpSound.dispose();
	}
	
}
