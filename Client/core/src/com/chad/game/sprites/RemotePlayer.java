package com.chad.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.chad.game.UltimateChad;


public class RemotePlayer extends Sprite  {
	 public World world;
	 private String remoteID;
	 private String name;
	 public String Color;
	 SpriteBatch batch;
	 private Label label;
	 private Group group;
	 LabelStyle labelStyle;
	 
	 public RemotePlayer(World world, String remoteID, String name, Texture playerTexture) {
		 super(playerTexture);
		 this.world = world;
		 this.remoteID = remoteID;
		 this.name = name;
		 defineRemotePlayer();
		 
	 }

	 // Set initial positions and down-scale the remote player to fit the world
	 public void defineRemotePlayer() {
		 // The start position of the player
		 this.setPosition(96/UltimateChad.PPM, 96/UltimateChad.PPM);
		 // Down-scaling
		 this.setSize(this.getWidth()/100f, (float)this.getHeight()/100f);
	 }
	
	 public void reDefineRemotePlayer(float x, float y) {
		// Update the remote player position
		this.setPosition((float)x - this.getWidth()/2,(float)(y - this.getHeight()/2));
	 }
	 
	 // Getter for the ID of the remote client
	 public String getID() {
		 return remoteID;
	 }
	 
	 // Getter for the ID of the remote client
	 public String getName() {
		 return name;
	 }
}