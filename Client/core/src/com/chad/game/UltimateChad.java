package com.chad.game;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;
import org.jspace.SequentialSpace;
import org.jspace.Space;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.chad.game.screens.MenuScreen;
import com.chad.game.screens.PlayScreen;

public class UltimateChad extends Game {
	// Server variables
	public static final String ip = "localhost";
	public static final String port = "31420";
	public static String clientID = "";
	private SequentialSpace userSpace = new SequentialSpace();
	private SequentialSpace remoteConnectSpace;
	RemoteSpace userIn;
	RemoteSpace userOut;
	
	// Game variables
	public static float PPM = 100;
	public static int viewWidth = 1440;
	public static int viewHeight = 810;
	public SpriteBatch batch;
	public static String currentWorld;
	private PlayScreen playScreen;
	
	@Override
	public void create () {
		System.out.println("-|Client|-");
		
		// Game logic begins
		// Create a sprite batch to hold all sprites to render
		batch = new SpriteBatch();
		// Game logic ends
		
		// Create remote space
		remoteConnectSpace = new SequentialSpace();
		
		
		//Start the menu screen
		MenuScreen menuScreen = new MenuScreen(this);
		setScreen(menuScreen);
		
	}
	
	public void serverLogic(String ip, String port, String name) {
		
		// Server logic starts
		// Server connection
		try {
			// Connect to the server
			System.out.println("Trying to connect");
			RemoteSpace lobby = new RemoteSpace("tcp://" + ip + ":" + port + "/lobby?keep");
			System.out.println("Connection succesful!");
			
			// Tell the server we're connected, and get the unique ID generated for us
			try {
				
				lobby.put("clientConnect", name);
				Object[] input = lobby.get(new ActualField("clientID"), new FormalField(String.class), new FormalField(String.class));
				clientID = (String) input[1];
				remoteConnectSpace.put("playerColor", input[2].toString());
				
				
				// If max client count is reached, cancel the connection and close the client
				if(clientID.equals("errorTooManyClients")) {
					// Run cleanup and exit the application
					System.out.println("Error: Too many clients connected!");
					create();
					return;
				}
			} catch (InterruptedException e) {}
			
			// Get unique spaces
			userIn = new RemoteSpace("tcp://" + ip + ":" + port + "/" + clientID + "In?keep");
			userOut = new RemoteSpace("tcp://" + ip + ":" + port + "/" + clientID + "Out?keep");
			System.out.println("Achieved personal space! Unique ID is " + clientID);
			
			// Create the player screen
			playScreen = new PlayScreen(this, remoteConnectSpace, userIn, userOut, name, "UltimateChadLobby");
			
			// Create butlers
			new Thread(new butlerReciever(userIn, this)).start();
			new Thread(new butlerPingReceiver(userIn, this)).start();

			
			// Get the new world
			try {
				Object[] world = remoteConnectSpace.get(new ActualField("newWorld"), new FormalField(String.class));
				//System.out.println(world[1].toString());
				
				// Change world
				playScreen.setWorld(world[1].toString());
				setScreen(playScreen);
			} catch (InterruptedException e) {}
			
		} catch (UnknownHostException e) {
			System.out.println("Unknown host!");
		} catch (IOException e) {
			System.out.println("Request timed out!");
		}
		// Server logic ends
	}
	
	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		System.out.println("MANUAL CLIENT EXIT");
		batch.dispose();
	}
	
	public void dataSwitch(Object[] data) {
		String opcode = data[0].toString();
		
		
		switch(opcode) {
			case("positionUpdate"):
				if(this.playScreen != null) {
					playScreen.setRemotePlayerPosition((String) data[1], (float) data[2], (float)data[3]);
				}
			
				break;
			case("100"): // Ping received
				try {
					userOut.put("pingToServer", -999, -999, "pingOK");
				} catch (InterruptedException e) {}
				break;
			case("clientWon"):
				playScreen.currentWorld = (data[2].toString());
				playScreen.winnerName = data[3].toString();
				playScreen.lookForNewWorld = true;
				break;
			case("clientReady"):
				System.out.println("client ready");
				playScreen.currentWorld = (data[2].toString());
				playScreen.lookForNewWorld = true;
				break;
			case("newClientConnection"):
				System.out.println("new Client connnect");
				System.out.println("\tClient " + data[1] + " connected.");
				// Creation of remote instances of players
				try {
					remoteConnectSpace.put("clientConnected", data[1].toString(), data[2].toString(), data[3].toString());
				} catch (InterruptedException e1) {} // id, name, color
			
				break;
			case("clientDisconnect"):
				System.out.println("client Disconnect");
				System.out.println("\tClient " + data[1] + " disconnected.");
				// Creation of remote instances of players
				try {
					remoteConnectSpace.put("clientDisconnect", data[1].toString());
				} catch (InterruptedException e1) {}
			
				break;
			case("playerColor"):
				System.out.println("player Color");
				break;
			case("connectedClients"):
				try {
				System.out.println("connectedClients array");
				ArrayList<String> clientList = (ArrayList<String>) data[2];
				String[][] playerInfoList = (String[][]) data[3];
				String currentWorld = (String) data[1];
				remoteConnectSpace.put("newWorld", currentWorld);
	
				// Creating remote instances that already have a connection to the server
				System.out.println("Creating remote instances.");
				if(!clientList.isEmpty()) {
					for(int i = 0; i < clientList.size(); i++) {
						if(!clientList.get(i).equals(clientID)) {
							System.out.println(clientList.get(i));
							String[] nameColor = getNameAndColor(playerInfoList, clientList.get(i));
							// Creation of remote instances of players
							remoteConnectSpace.put("clientConnected", clientList.get(i), nameColor[0], nameColor[1]);
						}
					}
				}
				} catch (InterruptedException e) {}
				break;
				
				default:
				System.out.println("REACHED DEFAULT! " + opcode);
					
			
		}
		
	}
	private String[] getNameAndColor(String[][] playerInfoList, String clientID) {
		for(int i = 0; i < playerInfoList[0].length; i++) {
			if(playerInfoList[i][0].equals(clientID)) {
				return new String[] {playerInfoList[i][1], playerInfoList[i][2]};
			}
		}
		return new String[] {"empty", "error"}; // No player is found
	}
	
}

// Manages connections to the server, and creates remote instances of the player
class butlerReciever implements Runnable{
	private Space userIn;
	private UltimateChad ultimateChad;
	
	public butlerReciever(Space userIn, UltimateChad ultimateChad) {
		this.userIn = userIn;
		this.ultimateChad = ultimateChad;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Object[] data = userIn.get(new FormalField(String.class), new FormalField(Object.class),
						new FormalField(Object.class), new FormalField(Object.class));
				ultimateChad.dataSwitch(data);
			} catch (InterruptedException e) {}
			catch(IllegalStateException dc) {}
		}	
	}
}

class butlerPingReceiver implements Runnable {
	private Space userIn;
	private UltimateChad ultimateChad;
	
	public butlerPingReceiver(Space userIn, UltimateChad ultimateChad) {
		this.userIn = userIn;
		this.ultimateChad = ultimateChad;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Object[] data = userIn.get(new ActualField(100), new FormalField(Object.class),
						new FormalField(Object.class), new FormalField(Object.class));
				ultimateChad.dataSwitch(data);
			} catch (InterruptedException e) {}
			catch(IllegalStateException dc) {}
		}
	}
}


