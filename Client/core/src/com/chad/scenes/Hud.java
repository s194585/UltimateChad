package com.chad.scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.chad.game.UltimateChad;


public class Hud {
	public Stage stage;
	private Viewport viewPort;
	
	private long speedRunTime = 0;
	String winningClient = "";
	Label winningClientLabel;
	Label speedRunCounter;
	
	Label countdownLabel;
	
	
	public Hud(SpriteBatch sb) {
		viewPort = new FitViewport(UltimateChad.viewWidth, UltimateChad.viewHeight, new OrthographicCamera());
		stage = new Stage(viewPort, sb);
		
		Table table = new Table();
		table.top();
		table.setFillParent(true);
		
		LabelStyle labelStyle = new Label.LabelStyle(new BitmapFont(), Color.WHITE);
		
		speedRunCounter = new Label("0.00", labelStyle);
		winningClientLabel = new Label(winningClient, labelStyle);
		
		winningClientLabel.setFontScale(4f);
		speedRunCounter.setFontScale(2f);
		
		table.add(speedRunCounter).expandX().padTop(20);
		table.row();
		table.add(winningClientLabel).expandX().padTop(20);
		stage.addActor(table);
	}
	
	public void update(float deltaTime) {
		speedRunCounter.setText(String.format("%.2f",(float)(System.currentTimeMillis()-speedRunTime)/1000));
	}
	
	public void setWinningClient(String clientName) {
		winningClientLabel.setText(clientName.toUpperCase());
	}
	public String getWinnerName() {
		return winningClientLabel.getText().toString();
	}
	
	public void resetSpeedRunTime() {
		speedRunTime = System.currentTimeMillis();
		speedRunCounter.setText(String.format("%.2f",(float)(System.currentTimeMillis()-speedRunTime)/1000));
	}
	
	public void speedRunEnabled(boolean enabled) {
		speedRunCounter.setVisible(enabled);
	}

}
