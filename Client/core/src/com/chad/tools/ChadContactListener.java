package com.chad.tools;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.chad.game.sprites.Player;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ChadContactListener implements ContactListener {

	@Override
	public void beginContact(Contact contact) {
		Fixture fixA = contact.getFixtureA();
		Fixture fixB = contact.getFixtureB();
		if (fixA.getUserData() == "death" || fixB.getUserData() == "death") {
			Fixture player = fixA.getUserData() == "death" ? fixB : fixA;
			if (player.getUserData() != null && Sprite.class.isAssignableFrom(player.getUserData().getClass())) {
				((Player) player.getUserData()).killPlayer();
			}

		}
		if (fixA.getUserData() == "win" || fixB.getUserData() == "win") {
			Fixture player = fixA.getUserData() == "win" ? fixB : fixA;
			if (player.getUserData() != null && Sprite.class.isAssignableFrom(player.getUserData().getClass())) {
				((Player) player.getUserData()).notifyServer("clientWon");
			}

		}
		if (fixA.getUserData() == "play" || fixB.getUserData() == "play") {
			Fixture player = fixA.getUserData() == "play" ? fixB : fixA;
			if (player.getUserData() != null && Sprite.class.isAssignableFrom(player.getUserData().getClass())) {
				((Player) player.getUserData()).notifyServer("clientReady");
			}

		}
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

}
